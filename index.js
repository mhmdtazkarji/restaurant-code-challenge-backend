import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();
const app = express();
const port = process.env.PORT;

console.log(port);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(port, () => console.log(`server running on port ${port}`));
